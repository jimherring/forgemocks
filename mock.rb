#!/usr/bin/env ruby

require 'nokogiri'
require 'sinatra'
require 'singleton'
require 'sqlite3'

class DB
  include Singleton

  attr_accessor :db

  def initialize
    @db || @db = SQLite3::Database.new('mock-state.db')
  end

  def rebuild!
    unless table_exists? 'workday'
      db.execute <<-SQL
        create table workday (
          wid varchar(64),
          user_id varchar(64),
          fname varchar(64),
          lname varchar(64)
        );
      SQL
    end

    db.execute 'DELETE FROM workday'
  end

  def seed
    db.execute 'INSERT INTO workday VALUES (?,?,?,?)', '7d6174890ba34da28b7ded2e8136e219', 'jcole', 'Jill', 'Cole'
  end

  def table_exists?(table)
    db.get_first_value %q(SELECT name FROM sqlite_master WHERE type='table' AND name=?), table
  end

  def wd_fetch_worker(wid=nil)
    row = if wid
      db.get_first_row 'SELECT * FROM workday WHERE wid=?', wid
    else
      db.get_first_row 'SELECT * FROM workday'
    end

    {
      'wid'     => row[0],
      'user_id' => row[1],
      'fname'   => row[2],
      'lname'   => row[3]
    }
  end

  def wd_update_worker(attrs)
    wid = attrs.delete 'wid'
    fields = []
    values = []
    attrs.keys.each do |k|
      fields << "#{k}=?"
      values << attrs[k]
    end
    db.execute "UPDATE workday SET #{fields.join(',')} WHERE wid=?", [values, wid].flatten
  end
end

db = DB.instance
db.rebuild!
db.seed

# test handler
get '/test' do
  "Mock Active"
end


# workday handler
post '/path/to/Human_Resources/v23.2' do
  content_type :xml
  db = DB.instance
  doc = parse_body request
  env_body = doc.xpath('//Body').first.first_element_child
  action = env_body.name
  puts "workday action: #{action}"

  user = db.wd_fetch_worker

  case action
  when 'Get_Workers_Request'
    return serve_response 'workday-get-users', { 'user' => user }

  when 'Change_Legal_Name_Request'
    fname = doc.xpath('//Name_Data/First_Name').first.content
    lname = doc.xpath('//Name_Data/Last_Name').first.content
    db.wd_update_worker 'wid' => user['wid'], 'fname' => fname, 'lname' => lname
    user = db.wd_fetch_worker user['wid']
    return serve_response 'workday-legal-name-change', { 'user' => user }

  when 'Workday_Account_for_Worker_Update'
    user_id = doc.xpath('//Workday_Account_for_Worker_Data/User_Name').first.content
    db.wd_update_worker 'wid' => user['wid'], 'user_id' => user_id
    return serve_response 'workday-account'
  end
end


# epic handler
post '/epic/PersonnelManagement.svc/basic' do
  content_type :xml
  doc = parse_body request

  # authorize request
  username = doc.xpath('//Header//Username').first.content
  password = doc.xpath('//Header//Password').first.content
  unless username == 'EMP:my-username' && password == 'my-password'
    puts "Epic authentication failed: #{username}/#{password}"
    return serve_response('epic-auth-fail')
  end

  env_body = doc.xpath('//Body').first.first_element_child
  action = env_body.name
  puts "epic action: #{action}"

  case action
  when 'ViewUser'
    user_id = env_body.xpath('//UserID').first.content
    template = user_id == '-1' ? 'epic-auth' : 'epic-view-user'
    return serve_response template

  when 'UpdateUser'
    return serve_response 'epic-update-user'

  when 'SetUserPassword'
    return serve_response 'epic-set-user-password'

  else
    return serve_response 'epic-fail'
  end
end


def parse_body(req)
  req.body.rewind
  doc = Nokogiri::XML req.body.read
  doc.remove_namespaces!
  doc
end

def get_binding(data, ops_orig)
  ops = ops_orig.dup
  op = ops.shift
  val = data[op]

  if val.kind_of? Hash
    return get_binding val, ops
  
  else
    return val
  end
end

def serve_response(action, binds={})
  puts "Serving response #{action}"
  template = ""
  File.open("responses/#{action}.xml", "rb") do |fh|
    template = fh.read
  end

  template.gsub! /\{\{\s*epic_header\s*\}\}/i, epic_header()

  output = template.gsub(/\{\{(.+?)\}\}/) do |m|
    ops = $1.strip.split('.')
    get_binding binds, ops
  end

  return output
end


def epic_header
  ts = Time.now.utc
  exp = ts + 5*60 # expires in 5 mins
  return(<<-"EOT")
    <s:Header>
      <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <u:Timestamp u:Id="_0">
          <u:Created>#{ts.strftime "%Y-%m-%dT%H:%M:%S.%3NZ"}</u:Created>
          <u:Expires>#{exp.strftime "%Y-%m-%dT%H:%M:%S.%3NZ"}</u:Expires>
        </u:Timestamp>
      </o:Security>
    </s:Header>
  EOT
end
