# README #

Forgemocks is a small [Sinatra](http://www.sinatrarb.com/) web application for serving mock responses to Forge tests.


## Architecture ##

Forgemocks currently lives on mocks.netspill.com. Requests are handled by Nginx and then proxied back to the Sinatra app on port 4567. Because some connectors have differing requirments there are several Nginx vhosts active.

The system is built with an ansible playbook that manages everything outside of the Forgemocks code. This included vhost files and certificates.

The app server can be started via: ```sudo service forgemocks-app start```.


## Certificates ##

The system uses self-signed certificates on it's endpoints. In order for this to work under java you will need to import the certificates or replace your current cacerts file with the mockserver-cacerts.jks file from the repository. The steps for replacement on an Ubuntu system are:

```
$ sudo cp mockserver-cacerts.jks /etc/ssl/certs/java/
$ cd $JAVA_HOME/jre/lib/security
$ sudo rm cacerts
$ sudo ln -s /etc/ssl/certs/java/mockserver-cacerts.jks cacerts
```


## Configuring Mocks ##

Please note that this project is for serving mock responses. These are only useful in the context of simple, automated testing.

Mocks.rb is a simple, monolithic Sinatra app. It uses Nokogiri to parse request xml and then serves canned responses from the responses/ directory. Mock request formats can vary wildly between connectors so there are no hard and fast rules for handling them. It is suggested to look at the epic handler code as a starting point.


## Connectors ##

If your favorite connector is not listed here then it is not included.

### AzureAd (WIP) ###

To use this connector you must edit your hosts file such that login.windows.net points to the address of the mock server.

* Domain: login.windows.net
* Port: 443

No traffic dumps available for this connector.

### Epic Health ###

* Domain: mocks.netspill.com
* Port: 1443
* Request Path: /epic/PersonnelManagement.svc/basic

### Workday ###

* Domain: mocks.netspill.com
* Port: 1443
* Request Path: /path/to/Human_Resources/v23.2